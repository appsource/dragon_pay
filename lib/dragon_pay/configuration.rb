class DragonPay::Configuration

  attr_accessor :test_mode, :merchant_id, :secret_key, :currency

  def initialize
    @test_mode   = true
    @merchant_id = 'APPSOURCE'
    @secret_key  = 'Jy$mV8qL'
    @currency    = 'PHP'
  end

  def payment_switch_domain
    test_mode ? 'http://test.dragonpay.ph' : 'https://secure.dragonpay.ph'
  end

end
