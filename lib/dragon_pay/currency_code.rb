class DragonPay::CurrencyCode
  class << self

    { 
      philippine_peso: 'PHP',
      us_dollar: 'USD'

    }.each do |key, value|

      define_method(key) do
        value
      end

    end

  end
end
